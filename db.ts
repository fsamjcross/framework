import mongoose from "mongoose";

mongoose.connect('mongodb://root:example@mongo:27017/', {});

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Connected to database');
});

export default db
