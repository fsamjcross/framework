import { Router, Request, Response, NextFunction } from 'express';
import { Model, Document } from 'mongoose';

class CrudlController<T extends Document> {
    constructor(private model: Model<T>) {}

    public async list(req: Request, res: Response, next: NextFunction) {
        console.log('test');
        try {
            const items = await this.model.find();
            res.status(200).json(items);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    }

    public async create(req: Request, res: Response, next: NextFunction) {
        const item = new this.model(req.body);
        try {
            const savedItem = await item.save();
            res.status(201).json(savedItem);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    }

    public async read(req: Request, res: Response, next: NextFunction) {
        try {
            const item = await this.model.findById(req.params.id);
            if (!item) {
                return res.status(404).json({ error: 'Item not found' });
            }
            res.status(200).json(item);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    }

    public async update(req: Request, res: Response, next: NextFunction) {
        try {
            const item = await this.model.findByIdAndUpdate(req.params.id, req.body, { new: true });
            if (!item) {
                return res.status(404).json({ error: 'Item not found' });
            }
            res.status(200).json(item);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    }

    public async delete(req: Request, res: Response, next: NextFunction) {
        try {
            const item = await this.model.findByIdAndRemove(req.params.id);
            if (!item) {
                return res.status(404).json({ error: 'Item not found' });
            }
            res.status(204).send();
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
        }
    }
    static router(): Router {
        // @ts-ignore
        const instance = new this();
        const router = Router();
        router.get('/', instance.list.bind(instance));
        router.post('/', instance.create.bind(instance));
        router.get('/:id', instance.read.bind(instance));
        router.put('/:id', instance.update.bind(instance));
        router.delete('/:id',instance.delete.bind(instance));
        return router;
    }
}

export default CrudlController;
